#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class Renderer {
public:
	SDL_Window * window;
	SDL_Surface * surface;
	SDL_Renderer * renderer;
	TTF_Font * font;

	static Renderer * getInstance();

	void setGlobalFontFromFile(std::string filename, int ptsize);

	void cleanup();

private:
	Renderer();
	~Renderer();

	static Renderer * instance;
};