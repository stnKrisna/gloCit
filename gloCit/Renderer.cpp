#include "Renderer.h"

Renderer * Renderer::instance;

Renderer::Renderer() {
	this->surface = nullptr;
	this->window = nullptr;
	this->renderer = nullptr;
}

Renderer::~Renderer() {
	this->surface = nullptr;
	SDL_DestroyWindow(this->window);
	SDL_DestroyRenderer(this->renderer);
	this->window = nullptr;
	this->renderer = nullptr;

	TTF_CloseFont(this->font);
	this->font = NULL;
}

void Renderer::cleanup() {
	delete(Renderer::instance);
}

Renderer * Renderer::getInstance() {
	if (Renderer::instance == nullptr) {
		Renderer::instance = new Renderer();
	}

	return Renderer::instance;
}

void Renderer::setGlobalFontFromFile(std::string filename, int ptsize) {
	this->font = TTF_OpenFont(filename.c_str(), ptsize);
}