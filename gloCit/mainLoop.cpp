#include "mainLoop.h"
#include "Renderer.h"

#include "Nav.h"
#include "Content.h"

#include "json.h"

#include <map>

static Nav * nav = nullptr;
static std::map<std::string, Content *> contents;

void loadContent(App * app) {
	int entrySize = app->data.at("entry").size();

	for (int i = 0; i < entrySize; i++) {
		std::string currentEntry = app->data.at("entry")[i];
		nlohmann::json contentData = app->data.at("content").at(currentEntry);
		nlohmann::json imagesData = contentData.at("images");
		Content * content = new Content(contentData.at("title"), contentData.at("paragraphs"), imagesData);

		contents.insert(std::make_pair(currentEntry, content));
	}
}

void doLoop(App * app) {
	// Load content when not initialized
	if (nav == nullptr) {
		nav = new Nav(app);
		loadContent(app);
	}

	SDL_Event e;
	Renderer * renderer = Renderer::getInstance();
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT)
		{
			app->quit = true;
		}
	}

	// Clear screen
	SDL_SetRenderDrawColor(renderer->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer->renderer);

	// Draw content
	if (contents.find(app->state) != contents.end()) {
		contents.find(app->state)->second->render();
	} else {
		// Content state not found...
	}


	// Draw nav bar
	nav->buttonActions();
	nav->drawNav();

	// Update screen
	SDL_RenderPresent(renderer->renderer);
}

void cleanLoop() {
	delete nav;

	std::map<std::string, Content *>::iterator it;
	for (it = contents.begin(); it != contents.end(); it++) {
		delete it->second;
	}
	contents.empty();
}