#include "Content.h"
#include "Renderer.h"

Content::Content(std::string title, std::vector<std::string> paragraphs, nlohmann::json images) {
	titleFont = TTF_OpenFont("res/Ubuntu-B.ttf", 45);
	titleObject = new LTextureText(titleFont);
	titleObject->loadFromRenderedText(title, {0,0,0});

	// Render paragraph texts
	Renderer * renderer = Renderer::getInstance();
	paragraphCount = paragraphs.size();
	paragraphBody = new LTextureText*[paragraphCount];
	for (unsigned int i = 0; i < paragraphCount; i++) {
		paragraphBody[i] = new LTextureText(renderer->font);
		paragraphBody[i]->loadFromRenderedText(paragraphs[i], {0,0,0}, 300);
	}

	this->images = images;
	this->imagesCount = images.size();
	this->imagesObject = new LTexture*[imagesCount];
	for (unsigned int i = 0; i < imagesCount; i++) {
		std::string imageLoc = images[i].at("loc");
		int w = -1;
		int h = -1;

		if (images[i].contains("w") && images[i].contains("h")) {
			try {
				w = images[i].at("w");
				h = images[i].at("h");
			}
			catch (std::exception e) {
				w = h = -1;
			}
		}

		if (w == -1 || h == -1) {
			this->imagesObject[i] = new LTexture();
		}
		else {
			this->imagesObject[i] = new LTexture(w, h);
		}
		this->imagesObject[i]->loadFromFile(imageLoc);
	}
}

Content::~Content() {
	for (unsigned int i = 0; i < imagesCount; i++) {
		delete imagesObject[i];
	}
	delete imagesObject;
	imagesObject = nullptr;

	for (unsigned int i = 0; i < paragraphCount; i++) {
		delete paragraphBody[i];
	}
	delete paragraphBody;
	paragraphBody = nullptr;
}

std::string Content::getTitle() {
	return title;
}

void renderTitle(LTextureText * titleObject) {
	titleObject->render(10,10);
}

void Content::render() {
	Renderer * renderer = Renderer::getInstance();

	// Render title
	renderTitle(titleObject);

	// Render paragraphs
	int nextY_paragraph = 20 + titleObject->getHeight() + 20;
	for (unsigned int i = 0; i < paragraphCount; i++) {
		paragraphBody[i]->render(10, nextY_paragraph);
		nextY_paragraph += 20 + paragraphBody[i]->getHeight();
	}

	// Render Image
	for (unsigned int i = 0; i < imagesCount; i++) {
		int x = images[i].at("x");
		int y = images[i].at("y");
		this->imagesObject[i]->render(x, y);
	}
}