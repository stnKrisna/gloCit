#pragma once
#include "App.h"
#include "LTextureText.h"
#include "LTexture.h"
#include <vector>

class Nav {
public:
	Nav(App * app);
	~Nav();

	void drawNav();
	void buttonActions();

private:
	bool mouseHover(SDL_Rect rect);
	bool mouseClick(int button);

	App * app;

	std::vector<LTextureText *> buttons_text;
	TTF_Font * buttonFont = nullptr;
	LTexture * bottomBar = nullptr;
	SDL_Cursor* cursor = nullptr;
};