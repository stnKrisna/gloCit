#pragma once

#include "LTexture.h"
#include <SDL2/SDL_ttf.h>

class LTextureText : public LTexture {
public:
	//Initializes variables
	LTextureText(TTF_Font * gFont);

	//Deallocates memory
	~LTextureText();

	//Creates image from font string
	bool loadFromRenderedText(std::string textureText, SDL_Color textColor);
	bool loadFromRenderedText(std::string textureText, SDL_Color textColor, unsigned int length);
private:
	TTF_Font *gFont;
};