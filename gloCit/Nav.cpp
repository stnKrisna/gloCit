#include "Nav.h"
#include <iostream>
#include "Renderer.h"
#include <string>

#define NAV_TEXT_PADDING 10
#define NORMAL_COLOR {0, 0, 0}
#define HOVER_COLOR {80, 80, 80}

Nav::Nav(App * app) {
	this->app = app;

	int entrySize = app->data.at("entry").size();
	SDL_Color textColor = NORMAL_COLOR;
	Renderer * renderer = Renderer::getInstance();

	this->buttonFont = TTF_OpenFont("res/Ubuntu-B.ttf", 20);

	for (int i = 0; i < entrySize; i++) {
		std::string currentEntry = app->data.at("entry")[i];
		std::string buttonText = app->data.at("content").at(currentEntry).at("title");
		LTextureText * text = new LTextureText(this->buttonFont);
		text->loadFromRenderedText(buttonText, textColor);

		this->buttons_text.push_back(text);
	}

	this->bottomBar = new LTexture();
	this->bottomBar->loadFromFile("res/bottom.bmp");

	app->state = app->data.at("entry")[0];
}

Nav::~Nav() {
	for (unsigned int i = 0; i < this->buttons_text.size(); i++) {
		delete this->buttons_text[i];
	}

	this->buttons_text.clear();
}

bool Nav::mouseHover(SDL_Rect rect) {
	int mouseX = 0;
	int mouseY = 0;

	SDL_GetMouseState(&mouseX, &mouseY);

	if (mouseX < rect.x || mouseX > rect.x + rect.w)
		return false;
	
	if (mouseY < rect.y || mouseY > rect.y + rect.h)
		return false;

	return true;
}

bool Nav::mouseClick(int button) {
	return SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(button);
}

void Nav::buttonActions() {
	Renderer * renderer = Renderer::getInstance();

	// Set to default cursor
	cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);

	int nextPosX = NAV_TEXT_PADDING;
	int posY = SDL_GetWindowSurface(renderer->window)->h - this->buttons_text[0]->getHeight() - NAV_TEXT_PADDING;

	for (unsigned int i = 0; i < this->buttons_text.size(); i++) {
		if (mouseHover({ nextPosX - NAV_TEXT_PADDING, posY - NAV_TEXT_PADDING, buttons_text[i]->getWidth() + (NAV_TEXT_PADDING * 2), buttons_text[i]->getHeight() + NAV_TEXT_PADDING })) {
			cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

			if (mouseClick(SDL_BUTTON_LEFT)) {
				app->state = app->data.at("entry")[i];
			}
		}

		nextPosX += this->buttons_text[i]->getWidth() + (NAV_TEXT_PADDING * 2);
	}

	// Update cursor
	SDL_SetCursor(cursor);
}

void Nav::drawNav() {
	Renderer * renderer = Renderer::getInstance();

	int nextPosX = NAV_TEXT_PADDING;
	int posY = SDL_GetWindowSurface(renderer->window)->h - this->buttons_text[0]->getHeight() - NAV_TEXT_PADDING;

	this->bottomBar->render(0, posY - NAV_TEXT_PADDING);

	for (unsigned int i = 0; i < this->buttons_text.size(); i++) {
		if (mouseHover({ nextPosX - NAV_TEXT_PADDING, posY - NAV_TEXT_PADDING, buttons_text[i]->getWidth() + (NAV_TEXT_PADDING * 2), buttons_text[i]->getHeight() + NAV_TEXT_PADDING })) {
			cursor = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);

			SDL_Color textColor = HOVER_COLOR;
			std::string currentEntry = app->data.at("entry")[i];
			std::string buttonText = app->data.at("content").at(currentEntry).at("title");
			LTextureText * text_hover = new LTextureText(this->buttonFont);
			text_hover->loadFromRenderedText(buttonText, textColor);
			text_hover->render(nextPosX, posY);
			delete text_hover;
		} else {
			this->buttons_text[i]->render(nextPosX, posY);
		}

		nextPosX += this->buttons_text[i]->getWidth() + (NAV_TEXT_PADDING * 2);
	}
}

#undef NAV_TEXT_PADDING