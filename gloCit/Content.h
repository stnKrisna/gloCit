#pragma once
#include <string>
#include <list>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "LTexture.h"
#include "LTextureText.h"
#include "json.h"

class Content {
public:
	Content(std::string title, std::vector<std::string> paragraphs, nlohmann::json images);
	~Content();

	std::string getTitle();
	void render();

protected:
	std::string title;
	std::list<std::string> paragraphs;
	nlohmann::json images;

private:
	TTF_Font * titleFont;
	TTF_Font * paragraphFont;

	LTextureText * titleObject;
	LTextureText ** paragraphBody;
	LTexture ** imagesObject;

	unsigned int paragraphCount = 0;
	unsigned int imagesCount = 0;
};