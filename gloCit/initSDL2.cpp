#include "initSDL2.h"
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

bool createWindow (Renderer * renderer, std::string title, SDL_Rect rect, bool center) {
	renderer->window = SDL_CreateWindow(title.c_str(), rect.x, rect.y, rect.w, rect.h, 0);
	
	if (renderer->window == nullptr) {
		// Failed to create window
		return false;
	}
	else {
		renderer->renderer = SDL_CreateRenderer(renderer->window, -1, 0);
		renderer->surface = SDL_GetWindowSurface( renderer->window );
	}

	return true;
}

bool initSDL (unsigned int flag) {
	bool SDLInit = SDL_Init(flag) == 0;
	bool TTFInit = TTF_Init() == 0;
	//bool ImageInit = IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF) == 0;
	return SDLInit && TTFInit;// && ImageInit;
}