#pragma once

#include <SDL2/SDL.h>
#include <string>
#include "Renderer.h"

bool createWindow (Renderer * renderer, std::string title, SDL_Rect rect, bool center);
bool initSDL(unsigned int flag);
