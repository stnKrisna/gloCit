#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>

#include "initSDL2.h"
#include "Renderer.h"
#include "App.h"
#include "dataLoader.h"
#include "mainLoop.h"

#define FPS_CAP 1000/12

int WinMain(int argc, char * argv[]) {
	return main(argc, argv);
}

int main (int argc, char * argv[]) {
	
	if (initSDL(0)) {
		Renderer * renderer = Renderer::getInstance();
		createWindow(renderer, "Global Citizenship", { SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 500 }, true);
		renderer->setGlobalFontFromFile("res/Ubuntu-L.ttf", 12);
	}
	else {
		// SDL error
		std::cout << SDL_GetError();
	}

	// Initialize the app container
	App * app = new App;
	app->renderer = Renderer::getInstance();
	app->quit = false;

	// Read JSON data
	dataLoader(app, "res/data.json");

	// Perform main loop
	unsigned int frame_start;
	unsigned int frame_time;
	int sleepTime;
	while (!app->quit) {
		frame_start = SDL_GetTicks();
		doLoop(app); // mainLoop.h
		frame_time = SDL_GetTicks() - frame_start;
		sleepTime = frame_time + FPS_CAP;
		SDL_Delay(sleepTime < 0 ? 0 : FPS_CAP);
	}

	cleanLoop(); // mainLoop.h

	Renderer::getInstance()->cleanup();
	delete app;
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();

	return 0;
}