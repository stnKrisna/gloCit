#include "dataLoader.h"
#include <iostream>
#include <fstream>
#include <sstream>

void dataLoader (App * app, std::string filename) {
	std::ifstream dataFile(filename);

	if (dataFile.is_open()) {
		std::stringstream buffer;

		buffer << dataFile.rdbuf();
		std::string content = buffer.str();

		app->data = nlohmann::json::parse(content);
		dataFile.close(); // Dont forget to close the file
	}
	else {
		std::cerr << "Cannot read file: " << filename;
		exit(EXIT_FAILURE);
	}
}