#include "LTextureText.h"
#include "Renderer.h"
#include <windows.h>

LTextureText::LTextureText(TTF_Font * gFont) : LTexture()
{
	//Initialize
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	this->gFont = gFont;
}

LTextureText::~LTextureText()
{
	//Deallocate
	free();
}

bool LTextureText::loadFromRenderedText(std::string textureText, SDL_Color textColor, unsigned int length)
{
	//Get rid of preexisting texture
	free();

	SDL_Renderer * gRenderer = Renderer::getInstance()->renderer;

	//Render text surface
	SDL_Surface* textSurface;
	if (length == 0) {
		textSurface = TTF_RenderText_Blended(gFont, textureText.c_str(), textColor);
	}
	else {
		textSurface = TTF_RenderText_Blended_Wrapped(gFont, textureText.c_str(), textColor, length);
	}
	if (textSurface == NULL)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
		OutputDebugString("TTF: ");
		OutputDebugString(TTF_GetError());
		OutputDebugString("\n");
	}
	else
	{
		//Create texture from surface pixels
		mTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
		if (mTexture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
			OutputDebugString("SDL: ");
			OutputDebugString(SDL_GetError());
			OutputDebugString("\n");
		}
		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}

	//Return success
	return mTexture != NULL;
}

bool LTextureText::loadFromRenderedText(std::string textureText, SDL_Color textColor) {
	return loadFromRenderedText(textureText, textColor, 0);
}