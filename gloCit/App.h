#pragma once

#include "Renderer.h"
#include "json.h"
#include <string>

typedef struct App {
	bool quit = false;
	Renderer * renderer = nullptr;
	nlohmann::json data;
	std::string state = "";
} App;